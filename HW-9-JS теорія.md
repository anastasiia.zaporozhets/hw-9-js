﻿
**1.Опишіть, як можна створити новий HTML тег на сторінці.**

Для того щоб створити новий тег потрібно використовувати метод document.createElement(tagName), де tagName - це ім'я тегу, який потрібно створити.

Після створення тега його можна додати до документа за допомогою методу appendChild() або insertBefore(), як це робиться з будь-яким іншим елементом:

**2.Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.**

Метод `insertAdjacentHTML` дозволяє додавати HTML-розмітку в певне місце відносно вказаного елемента. Перший параметр цієї функції визначає, куди саме буде вставлено нову HTML-розмітку. Це значення може бути одним із наступних:

\- `'beforebegin'`: розмітка буде вставлена перед початком елемента, до якого застосовується метод.

\- `'afterbegin'`: розмітка буде вставлена в самий початок елемента, до якого застосовується метод.

\- `'beforeend'`: розмітка буде вставлена в кінець елемента, до якого застосовується метод.

\- `'afterend'`: розмітка буде вставлена після елемента, до якого застосовується метод.

Цей метод зазвичай використовують для динамічного створення елементів та їх вставки в сторінку. При цьому можна будувати HTML-розмітку з різними елементами та стилізувати їх за допомогою CSS. Такий підхід дозволяє маніпулювати сторінкою без перезавантаження і оновлення її вмісту.

**3.Як можна видалити елемент зі сторінки?**

Є кілька способів видалити елемент зі сторінки. Один з найпростіших способів - використовувати метод `remove()`.

Інший спосіб - використовувати метод `parentNode.removeChild()`:

Обидва способи працюють, але метод `remove()` є більш сучасним та читабельним, тому зазвичай рекомендується використовувати саме його для видалення елементів зі сторінки.



