function renderList(arr, parent = document.body) {
    const listItems = arr.map(item => `<li>${item}</li>`).join('');
    const html = `<ul>${listItems}</ul>`;
    parent.insertAdjacentHTML('beforeend', html);
}

const listCity  = ["Kyiv", "Odesa", "Sumy", "Kharkiv", "Lviv"];

renderList(listCity);


// function renderList(arr, parent = document.body) {
//     const list = document.createElement('ul');
//
//     arr.forEach(item => {
//         const listItem = document.createElement('li');
//         listItem.textContent = item;
//         list.appendChild(listItem);
//     });
//
//     parent.appendChild(list);
// }
//
// const arr = ["Kyiv", "Odesa", "Sumy", "Kharkiv", "Lviv"];
// renderList(arr);



